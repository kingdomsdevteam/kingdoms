﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers
{
    /// <summary>
    /// The creator object that calculates all required values and sets up the necessary objects for a game.
    /// </summary>
    public partial class GameCreator
    {
        private static GameCreator instance = null;
        const byte numberOfPlayers = 5;
        private uint numberOfKnights;
        private uint numberOfSpearmen;
        private Random rnd = new Random();
        private string[] aiNames = new string[] { //Male names:
                                        "Baron Gwatkin",
                                        "King Umphrey",
                                        "Baron Emeric",
                                        "Lord Charles",
                                        "Sir Taff",
                                        "Monk Hemonnet",
                                        "Prince Consort Gauteron",
                                        "Bishop Fouque",
                                        "Viscount Terrowin",
                                        "Yeoman Jeremiah",
                                        //Female names:
                                        "Maiden Ysmay",
                                        "Queen Tetty",
                                        "Squire Isold",
                                        "Princess Consort Theffania",
                                        "Maid Jonetam",
                                        "Dame Cristan",
                                        "Viscountess Miriella",
                                        "Maid Johanna",
                                        "Dame Jocey",
                                        "Serf Adeliz" };

        private List<Entities.Land> landList = new List<Entities.Land>();
        private List<Entities.Player> playerList = new List<Entities.Player>();
        private GameCreator()
        {
        }

        /// <summary>
        /// The current instance of the GameCreator singleton object.
        /// </summary>
        public static GameCreator Instance
        {
            get
            {
                if (instance == null)
                    instance = new GameCreator();
                return instance;
            }
        }

        /// <summary>
        /// Creates a new GameController.
        /// </summary>
        public GameInfo StartNewGame(string playerName)
        {
            playerList.Add(new Entities.HumanPlayer(playerName, GenerateNewArmy(), PickStartingLand()));
            for (int i = 0; i < numberOfPlayers-1; i++)
            {
                playerList.Add(new Entities.AIPlayer(PickName(), GenerateNewArmy(), PickStartingLand()));
            }
            return new GameInfo(playerList, landList);
        }

        private Entities.Army GenerateNewArmy()
        {
            List<Entities.Unit> unitList = new List<Entities.Unit>();
            numberOfKnights = (uint)rnd.Next(5, 9);
            numberOfSpearmen = (uint)rnd.Next(40, 51);
            for (int i = 0; i < numberOfSpearmen; i++)
            {
                unitList.Add(new Entities.Spearman((ushort)rnd.Next(3, 11), (ushort)rnd.Next(3, 11), 100));
            }
            for (int i = 0; i < numberOfKnights; i++)
            {
                unitList.Add(new Entities.Knight((ushort)rnd.Next(3, 11), (ushort)rnd.Next(3, 11), 100));
            }

            return new Entities.Army(unitList);
        }

        private string PickName ()
        {
            string[] unusedNames = aiNames.Where(n => playerList.Count(p => p.Name == n) == 0).ToArray();
            return unusedNames[rnd.Next(0, unusedNames.Length)];
        }

        private List<Entities.Land> PickStartingLand()
        {
            if (landList.Count != numberOfPlayers)
            {
                landList.Clear();
                for (int i = 0; i < numberOfPlayers; i++)
                {
                    landList.Add(new Entities.Land());
                }
            }
            List<Entities.Land> vacantLand = landList.Where(n => !playerList.Any(p => p.Lands.Any(l => l == n))).ToList();
            List<Entities.Land> result = new List<Entities.Land>();
            result.Add(vacantLand[rnd.Next(0, vacantLand.Count)]);
            return result;
        }
    }
}
