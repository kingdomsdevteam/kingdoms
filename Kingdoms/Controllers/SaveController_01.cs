﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace Controllers
{
    /// <summary>
    /// Handles saving and loading of savegame files.
    /// </summary>
    public partial class SaveController
    {
        private static SaveController instance;

        private JsonSerializer s = new JsonSerializer();
        public static SaveController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SaveController();
                }
                return instance;
            }
        }
        private SaveController()
        {

        }
        /// <summary>
        /// Loads a specified savegame file.
        /// </summary>
        /// <param name="saveName">The path to the savegame file to load.</param>
        /// <returns></returns>
        public SaveGameData LoadGame(string saveName)
        {
            StreamReader sr = File.OpenText($"{saveName}.savegame");
            object saveData = s.Deserialize(sr, typeof(SaveGameData));
            JsonConvert.DeserializeObject(sr.rea, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All });
            sr.Close();
            return (SaveGameData)saveData;
        }

        /// <summary>
        /// Saves a game from a SaveGame object.
        /// </summary>
        /// <param name="save">Savegame to write to a file.</param>
        public void SaveGame(SaveGameData save)
        {
            StreamWriter sw = File.CreateText($"{save.SaveName}.savegame");
            s.Serialize(sw, save);
            sw.Close();
        }
    }

    /// <summary>
    /// An object representing a collection of savegame data.
    /// </summary>
    public struct SaveGameData
    {
        public string SaveName { get; set; }
        public List<Entities.Player> Players { get; set; }
        public List<Entities.Land> Lands { get; set; }

        /// <summary>
        /// Creates a new SaveGameData object for saving.
        /// </summary>
        /// <param name="saveName">The name of the file to save as.</param>
        /// <param name="players">The list of all players in the game, both human and AI.</param>
        /// <param name="lands">The last of available pieces of land in the game.</param>
        public SaveGameData(string saveName, List<Entities.Player> players, List<Entities.Land> lands)
        {
            SaveName = saveName;
            Players = players;
            Lands = lands;
        }
    }
}
