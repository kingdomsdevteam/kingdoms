﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers
{
    /// <summary>
    /// The primary game controller.
    /// </summary>
    public partial class GameController
    {
        #region Fields
        #endregion

        #region Properties
        private List<Entities.Player> playerList;

        /// <summary>
        /// A list of all players in the game.
        /// Used for turn management.
        /// </summary>
        public List<Entities.Player> PlayerList
        {
            get { return playerList; }
            set { playerList = value; }
        }
        
        private List<Entities.Land> landList;
        /// <summary>
        /// A list of all land in the game.
        /// Used to send as parameters to other methods.
        /// </summary>
        public List<Entities.Land> LandList
        {
            get { return landList; }
            set { landList = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates the primary game controller.
        /// </summary>
        public GameController(GameInfo gameInfo)
        {
            PlayerList = gameInfo.PlayerList;
            LandList = gameInfo.LandList;
        }
        #endregion

        /// <summary>
        /// Buys a number of units of a certain type.
        /// </summary>
        /// <param name="type">The type of units to purchase.</param>
        /// <param name="amount">The number of units to purchase.</param>
        /// <returns>The result of the purchase attempt.</returns>
        public PurchaseResult BuyUnits(Type type, int amount)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Ends the turn, and calculates runs the AI method.
        /// </summary>
        public void EndTurn()
        {
            //throw new System.NotImplementedException();
        }

        /// <summary>
        /// Initiates a battle.
        /// </summary>
        /// <param name="battle">The battle to fight.</param>
        /// <param name="didPlayerWin">A value indicating wether the player won or not.</param>
        public void ExecuteBattle(Entities.Battle battle, out bool didPlayerWin)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Attempts to invade a piece of land.
        /// </summary>
        /// <param name="land">The land to invade.</param>
        /// <param name="didPlayerWin">A value indicating whether the player won.</param>
        public void InvadeLand(Entities.Land land, out bool didPlayerWin)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Trains/upgrades a unit of a specified type.
        /// </summary>
        /// <param name="type">The type of unit to train/upgrade.</param>
        /// <returns>The result of the training attempt.</returns>
        public TrainingResult TrainUnits(Type type)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Exchanges units between a castle and the army.
        /// </summary>
        public void SwitchUnits()
        {
            throw new System.NotImplementedException();
        }
    }
}
