﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controllers
{
    /// <summary>
    /// An enum representing the result of a purchase attempt.
    /// </summary>
    public enum PurchaseResult
    {
        /// <summary>
        /// The player did not have enough money.
        /// </summary>
        NotEnoughMoney,
        /// <summary>
        /// The purchase was successful.
        /// </summary>
        Success,
        /// <summary>
        /// No more unit can be purchased.
        /// </summary>
        UnitCapReached
    }

    /// <summary>
    /// An enum representing the result of a unit training attempt.
    /// </summary>
    public enum TrainingResult
    {
        /// <summary>
        /// The player did not have enough money.
        /// </summary>
        NotEnoughMoney,
        /// <summary>
        /// The training attempt was successful.
        /// </summary>
        Success,
        /// <summary>
        /// The unit upgrade cap has been reached.
        /// </summary>
        UpgradeCapReached
    }
}