﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Controllers;

namespace ControllersTests
{
    /// <summary>
    /// Unit test for class Controllers.
    /// </summary>
    [TestClass]
    public class ControllerLayerTest
    {
        /// <summary>
        /// Tests the new game startup.
        /// </summary>
        [TestMethod]
        public void NewGameTest()
        {
            string name = "Lord Baraken";
            // Arrange

            //Create Game instance with GameInfo. GameInfo object can be reused.
            GameController gameInstance = new GameController(GameCreator.Instance.StartNewGame(name));

            //Perform game logic.

            // Act
            gameInstance.EndTurn();

            // Assert
            Assert.IsNotNull(GameCreator.Instance);
            Assert.IsNotNull(gameInstance);
            Assert.AreEqual(name, gameInstance.PlayerList[0].Name);
        }

        /// <summary>
        /// Tests the save game feature.
        /// </summary>
        [TestMethod]
        public void SaveLoadTest()
        {
            string name = "Lord Baraken";
            // Arrange

            //Create Game instance with GameInfo. GameInfo object can be reused.
            GameController gameInstance = new GameController(GameCreator.Instance.StartNewGame(name));

            //Perform game logic.

            // Act
            SaveController.Instance.SaveGame(new SaveGameData(name, gameInstance.PlayerList, gameInstance.LandList));
            SaveController.Instance.LoadGame(name);

            // Assert
            Assert.IsNotNull(GameCreator.Instance);
            Assert.IsNotNull(gameInstance);
            Assert.AreEqual(name, gameInstance.PlayerList[0].Name);
        }
    }
}
