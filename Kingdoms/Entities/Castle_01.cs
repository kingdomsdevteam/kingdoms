﻿using System.Collections.Generic;

namespace Entities
{
    public partial class Castle
    {
        private Army garrision;

        public Army Garrision
        {
            get
            {
                return garrision;
            }

            set
            {
                garrision = value;
            }
        }
    }
}