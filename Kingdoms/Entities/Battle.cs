﻿using System;
using CrossCutting;

namespace Entities
{
    /// <summary>
    /// Represents a battle over a contested land, between an invading army and the defending 
    /// garrision, and possibly also a defending army.
    /// </summary>
    public class Battle
    {
        #region Fields
        /// <summary>
        /// The army defending the land - may not be present when the land is invaded.
        /// </summary>
        private Army defendingArmy;

        /// <summary>
        /// The army invading the land.
        /// </summary>
        private Army attackingArmy;

        /// <summary>
        /// The land the attacking army invades.
        /// </summary>
        private Land contestedLand;

        /// <summary>
        /// The castle in the contested land.
        /// </summary>
        //private Castle_01 castle;

        /// <summary>
        /// The player invading the contested land with its army.
        /// </summary>
        private Object attackingPlayer;

        /// <summary>
        /// The player who 
        /// </summary>
        private Object defendingPlayer;
        #endregion

        #region Constructors
        /// <summary> 
        /// Creates a new instance of this class. When created, call the StartBattle method.    
        /// <param name="contestedLand">The land over which the battle occurs.</param>
        /// <param name="attackingPlayer">The player invading the contested land.</param>
        public Battle( Land contestedLand, Player attackingPlayer )
        {
            // Null checks:
            if ( contestedLand == null )
                throw new ArgumentNullException( $"{ErrorMessage.NullArgRef}, for parameter {nameof(contestedLand)}" );
            if ( attackingPlayer == null )
                throw new ArgumentNullException( $"{ErrorMessage.NullArgRef}, for parameter {nameof( attackingPlayer )}" );

            
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or privately sets the defending army.
        /// </summary>
        public Army DefendingArmy
        {
            get
            {
                return defendingArmy;
            }

            private set
            {
                if ( value == null )
                    throw new ArgumentNullException( ErrorMessage.NullArgRef );
                defendingArmy = value;
            }
        }

        /// <summary>
        /// Gets or privately sets the attacking army.
        /// </summary>
        public Army AttackingArmy
        {
            get
            {
                return attackingArmy;
            }

            set
            {
                if ( value == null )
                    throw new ArgumentNullException( ErrorMessage.NullArgRef );
                attackingArmy = value;
            }
        }

        /// <summary>
        /// Gets or sets the contested land.
        /// </summary>
        public Land ContestedLand
        {
            get
            {
                return contestedLand;
            }

            set
            {
                if ( value == null )
                    throw new ArgumentNullException( ErrorMessage.NullArgRef );
                contestedLand = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Starts the battle.
        /// </summary>
        /// <returns>The player who won the battle.</returns>
        public Player StartBattle()
        {
            Player winner = null;

            // Cool algorithm goes here...

            return winner;
        } 
        #endregion
    }
}