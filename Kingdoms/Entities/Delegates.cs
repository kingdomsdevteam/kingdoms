﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    /// <summary>
    /// Used to provide a method signature for handling a BattleResultEvent.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public delegate void BattleResultsHandler( object sender, BattleResultEventArgs args );
}
