﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Knight : Unit
    {
        #region fields
        private bool _isUsed = false;
        private ushort _BoostValue = 5;

        #endregion
        #region Constructors
        public Knight( ushort attackValue, ushort defenceValue, ushort cost ) : base( attackValue, defenceValue, cost )
        {

        }
        #endregion
        /// <summary>
        /// Boost the base stats of AttackValue
        /// </summary>
        void BoostMorale()
        {
            if (!_isUsed)
            {
                _isUsed = true;
                attackValue += _BoostValue;
            }
        }
    }
}
