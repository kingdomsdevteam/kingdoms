﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class HumanPlayer : Player
    {
        #region Constructors

        public HumanPlayer(string name, Army army, List<Land> land) : base(name, army, land)
        {

        }
        #endregion
    }
}
