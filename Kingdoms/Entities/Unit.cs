﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    /// <summary>
    /// A abstract class that contains the values that all different units share. 
    /// </summary>
    public class Unit
    {
        #region Fields
        protected ushort attackValue;
        protected ushort defenceValue;
        protected ushort cost;
        #endregion

        #region Constructors
        public Unit(ushort attackValue, ushort defenceValue, ushort cost)
        {
            this.attackValue = attackValue;
            this.defenceValue = defenceValue;
            this.cost = cost;
        }
        #endregion


    }
}
