﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Catapult : Unit
    {
        #region Fields
        private ushort numberOfStonesLeft;
        #endregion

        #region Constructors
        public Catapult( ushort attackValue, ushort defenceValue, ushort cost, ushort numberOfStonesLeft ) : base( attackValue, defenceValue, cost )
        {
            this.numberOfStonesLeft = numberOfStonesLeft;
        }
        #endregion


    }
}
