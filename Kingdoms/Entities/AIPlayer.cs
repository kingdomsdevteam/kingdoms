﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class AIPlayer : Player
    {
        #region Constructors

        public AIPlayer(string name, Army army, List<Land> land) : base(name, army, land)
        {

        }
        #endregion
    }
}
