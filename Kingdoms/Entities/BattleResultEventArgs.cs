﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class BattleResultEventArgs : EventArgs
    {
        private Player winner;
        private Player loser;
        private Land contestedLand;

        public Player Winner
        {
            get
            {
                return winner;
            }

            set
            {
                winner = value;
            }
        }

        public Player Loser
        {
            get
            {
                return loser;
            }

            set
            {
                loser = value;
            }
        }

        public Land ContestedLand
        {
            get
            {
                return contestedLand;
            }

            set
            {
                contestedLand = value;
            }
        }

        public BattleResultEventArgs(Player winner, Player loser, Land contestedland)
        {
            Winner = winner;
            Loser = loser;
            ContestedLand = contestedLand;
        }
        
    }
}
