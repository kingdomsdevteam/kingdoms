﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    /// <summary>
    /// A footsoldier unit with a spear.
    /// </summary>
    public class Spearman : Unit
    {
        /// <summary>
        /// Creates a spearman.
        /// </summary>
        /// <param name="atackValue">The damage the unit can inflict.</param>
        /// <param name="defenceValue">The damage the unit can absorb.</param>
        /// <param name="cost">The cost of buying the unit.</param>
        public Spearman(ushort atackValue, ushort defenceValue, ushort cost) : base(atackValue, defenceValue, cost)
        {

        }
    }
}
