﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    /// <summary>
    /// A class that contains the army of a player
    /// and is used to invade and do battle with another players army.
    /// </summary>
    public class Army
    {
        #region Fields
        protected List<Unit> units;
        #endregion

        #region Constructors
        public Army(List<Unit> units)
        {
            this.units = units;
        }
        #endregion

        #region Properties
        public List<Unit> Units
        {
            get
            {
                return units;
            }

            set
            {
                units = value;
            }
        }

        public void AddUnit(Unit unitToAdd)
        {
            try
            {
                units.Add(unitToAdd);
            }
            catch (ArgumentNullException)
            {
                throw;
            }
            
        } 

        public void RemoveUnit(Unit unitToRemove)
        {
            try
            {
                units.Remove(unitToRemove);
            }
            catch (ArgumentNullException)
            {
                throw;
            }
        }
        #endregion
    }
}
