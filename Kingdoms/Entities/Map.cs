﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    /// <summary>
    /// A class that will act as the gameboard, where the majority of the game will take place. 
    /// </summary>
    public class Map
    {
        #region Fields
        /// <summary>
        /// The list of the five lands in the game.
        /// </summary>
        private List<Land> land;
        /// <summary>
        /// The list of the four AI opponents in the game.
        /// </summary>
        private List<AIPlayer> aiPlayers;
        /// <summary>
        /// The player which the user will control during the game.
        /// </summary>
        private HumanPlayer humanPlayer;
        #endregion

        #region Constructors
        /// <summary>
        /// The constructor which creates a instance of the map, when a new game is started.
        /// </summary>
        /// <param name="land"></param>
        /// <param name="aiPlayers"></param>
        /// <param name="humanPlayer"></param>
        public Map( List<Land> land, List<AIPlayer> aiPlayers, HumanPlayer humanPlayer )
        {
            this.Land = land;
            this.aiPlayers = aiPlayers;
            this.HumanPlayer = humanPlayer;
        }
        #endregion

        #region Properties
        /// <summary>
        /// A property which gets and sets the value of the "land" field.
        /// </summary>
        public List<Land> Land
        {
            get
            {
                return land;
            }

            set
            {
                land = value;
            }
        }
        /// <summary>
        /// A property which gets and sets the value of the "aiPlayers" field.
        /// </summary>
        public List<AIPlayer> AiPlayers
        {
            get
            {
                return aiPlayers;
            }

            set
            {
                aiPlayers = value;
            }
        }
        /// <summary>
        /// A property which gets and sets the value of the "humanPlayer" field.
        /// </summary>
        public HumanPlayer HumanPlayer
        {
            get
            {
                return humanPlayer;
            }

            set
            {
                humanPlayer = value;
            }
        }
        #endregion
    }
}
