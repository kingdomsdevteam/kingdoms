﻿using CrossCutting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    /// <summary>
    /// The Player class which is the base class for the HumanPlayer and AIPlayer classes.
    /// </summary>
    public abstract class Player
    {
        #region Fields
        /// <summary>
        /// The name of the player.
        /// </summary>
        private string name;
        /// <summary>
        /// The army that the player controls.
        /// </summary>
        private Army army;
        /// <summary>
        /// A list that contains the players starting land and any land that the player successfully invades.
        /// </summary>
        private List<Land> lands;        
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="army"></param>
        /// <param name="lands"></param>
        public Player( string name, Army army, List<Land> lands )
        {
            try
            {
                Name = name;
                Army = army;
                Lands = lands;
            }
            catch ( ArgumentNullException )
            {
                throw;
            }
        }
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public Army Army
        {
            get
            {
                return army;
            }

            set
            {
                army = value;
            }
        }

        public List<Land> Lands
        {
            get
            {
                return lands;
            }

            set
            {
                if ( value == null )
                    throw new ArgumentNullException( $"{ErrorMessage.NullArgRef}, for setter in property {nameof( Lands )}." );
                lands = value;
            }
        }

        #endregion

        #region Methods
        public void Invade( Land landToInvade )
        {
            // null check:
            if ( landToInvade == null )
                throw new ArgumentNullException( $"{ErrorMessage.NullArgRef}, for parameter {nameof( landToInvade )}." );

            // Invasion means battle:
            Battle battle = new Battle( landToInvade, this );
            Player winner = battle.StartBattle();
            bool invasionAttempSuccess;

            //TODO: Comtemplate wether this selection should be in a try-catch:
            if ( winner == this )       // Attacker wins.
            {
                invasionAttempSuccess = true;
                SwapLands( winner, loser: landToInvade.Owner, landToSwap: landToInvade );
            }
            else                        // Defender wins.
            {
                invasionAttempSuccess = false;

            }

            //TODO: Signal GameController end of turn - maybe raise an event?
            
        }

        /// <summary>
        /// Swaps a centested land after a battle, from the loser to the winner.
        /// </summary>
        /// <param name="winner"></param>
        /// <param name="loser"></param>
        /// <param name="landToSwap"></param>
        private void SwapLands(Player winner, Player loser, Land landToSwap )
        {
            // If this method is only called from method Invade, is it really nescessary to null check?
            loser.Lands.Remove( landToSwap );
            winner.Lands.Add( landToSwap );
        }

        protected void OnBattleOver( BattleResultEventArgs args )
        {
            BattleOver( this, args );
        }
        #endregion

        #region Events
        public event BattleResultsHandler BattleOver; 
        #endregion
    }
}
