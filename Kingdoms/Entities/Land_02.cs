﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Land
    {
        private Castle castle;
        private Player owner;
        private Army currentArmy;


        public Castle Castle
        {
            get
            {
                return castle;
            }

            set
            {
                castle = value;
            }
        }

        public Player Owner
        {
            get
            {
                return owner;
            }

            set
            {
                owner = value;
            }
        }

        public Army CurrentArmy
        {
            get
            {
                return currentArmy;
            }

            set
            {
                currentArmy = value;
            }
        }
    }
}
